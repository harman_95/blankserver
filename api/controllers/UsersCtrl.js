'use strict';

var mongoose = require('mongoose'),
Address = mongoose.model('Address'),
path = require('path'),
NodeGeocoder = require('node-geocoder'),
fs = require('fs');


//address controller start here
exports.add_address = function(req, res) {
  var new_address = new Address({
    name: req.body.name,
    address1: req.body.address1,
    address2: req.body.address2,
    city: req.body.city,
    is_default: req.body.is_default,
    phone: req.body.phone,
    userId: req.body.userId
  });

new_address.save(function(err, address) {
  if (address == null){
     res.send({
      error: err,
      status: 0,
      data: null
    });
  }else{
    res.send({
      error: null,
      status: 1,
      data: address
    });
  }
});
};

exports.getAddresses = function(req, res) {
  Address.find({userId : req.params.userId }, function(err, addresses) {
       res.json(addresses); 
  });
};

exports.getDefaultAddress = function(req, res) {
  Address.findOne({userId : req.params.userId, is_default : '1' }, function(err, address) {
       res.json(address); 
  });
};

exports.delete_address = function(req, res) {
  Address.remove({_id : req.body.id}, function(err, task) {
     if (task == null){
       res.send({
        error: err,
        status: 0,
        data: null
      });
    }else{
      res.send({
        error: null,
        status: 1,
        data: task
      });
    }
    
  });
};

exports.makeDefaultAddress = function(req, res) {
  Address.update({ _id: req.body.aid }, { $set: { is_default: '1' }}, {new: true}, function(err, task) {
     if (task == null){
       res.send({
        error: err,
        status: 0,
        data: null
      });
    }else{
      Address.update({ userId: req.body.userId, _id: {$ne : req.body.aid} }, { $set: { is_default: '0' }}, {new: true}, function(err, updated) {
          res.send({
            error: null,
            status: 1,
            data: task
          });
      });
    }
    
  });
};