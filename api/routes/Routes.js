'use strict';
module.exports = function(app) {

var users = require('../controllers/UsersCtrl');
    app.route('/address')
    .get(users.getAddresses)
    .delete(users.delete_address)
    .post(users.add_address);

};
