'use strict';

var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var AddressSchema = new Schema({
    name: {
      type: String
    },
    address1: {
      type: String
    },
    address2: {
      type: String
    },
    city: {
      type: String
    },
    is_default: {
      type: String
    },
    phone: {
      type: String
    },
    userId: {
      type: String
    }
});

module.exports = mongoose.model('Address', AddressSchema);
